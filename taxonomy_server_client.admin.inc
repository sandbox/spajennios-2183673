<?php
/**
 * @file
 * Admin functions for taxonomy_server_client module.
 */

/**
 * Form builder.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function taxonomy_server_client_settings() {
  $form['taxonomy_server_client_server_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('Taxonomy Server Domain'),
    '#default_value' => variable_get('taxonomy_server_client_server_uri', ''),
    '#description' => t('The url of the taxonomy server, starting with schema (http, https...)'),
    '#attributes' => array('readonly' => 'readonly'),
  );

  $form['taxonomy_server_client_server_vocabulary_id_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Vocabulary by VID'),
    '#default_value' => variable_get('taxonomy_server_client_server_vocabulary_id_endpoint', ''),
    '#description' => t('The endpoint that returns vocabulary data by vid.'),
    '#attributes' => array('readonly' => 'readonly'),
  );

  $form['taxonomy_server_client_server_vocabulary_machine_name_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Vocabulary by Machine Name'),
    '#default_value' => variable_get('taxonomy_server_client_server_vocabulary_machine_name_endpoint', ''),
    '#description' => t('The endpoint that returns vocabulary data by machine name.'),
    '#attributes' => array('readonly' => 'readonly'),
  );

  $form['taxonomy_server_client_server_term_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Term by UUID'),
    '#default_value' => variable_get('taxonomy_server_client_server_term_endpoint', ''),
    '#description' => t('The endpoint that returns a term by uuid.'),
    '#attributes' => array('readonly' => 'readonly'),
  );

  $form['taxonomy_server_client_checkboxes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Client Behavior'),
    '#weight' => 5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  // @FIXME: Do this correctly.
  // Suggestion: Use definition list or other paired listing construct rather
  // than... if possible.
  $data = '<table>';
  $data .= '<tr><td><strong>Server Machine Name</strong></td><td><strong>Client Machine Name</strong></td></tr>';
  foreach (variable_get('taxonomy_server_client_mapping', array()) as $server => $client) {
    $data .= '<tr><td>' . $server . '</td><td>' . $client . '</td></tr>';
  }
  $data .= '</table>';

  $form['taxonomy_server_client_mapping'] = array(
    '#type' => 'markup',
    '#value' => $data,
  );

  $form['#validate'][] = 'taxonomy_server_client_settings_validate';
  $form['#submit'][] = 'taxonomy_server_client_settings_submit';
  return system_settings_form($form);
}

/**
 * Form validate handler.
 *
 * @param array $form
 *   Nested array of form elements that comprise the form.
 *
 * @param array $form_state
 *   A keyed array containing the current state of the form.
 *   The arguments that drupal_get_form() was originally called
 *   with are available in the array $form_state['build_info']['args'].
 *
 * @return bool
 *   Whether the validation passed or failed.
 */
function taxonomy_server_client_settings_validate($form, &$form_state) {
  $server_url = $form_state['values']['taxonomy_server_client_url'];
  if (valid_url($server_url, TRUE)) {
    return TRUE;
  }
  else {
    form_set_error('taxonomy_server_client_url', t('The provided URL is not valid.'));
    return FALSE;
  }
}

/**
 * Form submit handler.
 *
 * @param array $form
 *   Nested array of form elements that comprise the form.
 *
 * @param array $form_state
 *   A keyed array containing the current state of the form.  The arguments
 *   that drupal_get_form() was originally called with are available in the
 *   array $form_state['build_info']['args'].
 */
function taxonomy_server_client_settings_submit($form, &$form_state) {
  variable_set('taxonomy_server_client_settings', $form_state['values']['taxonomy_server_client_url']);
}
