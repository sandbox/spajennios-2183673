<?php

/**
 * @file
 * Documentation for Taxonomy Server Client module.
 */

/**
 * Notify other modules when a term is modified.
 *
 * @param object $term
 *   Fully loaded taxonomy term object
 *
 */
function hook_taxonomy_server_client_action($term) {
}
