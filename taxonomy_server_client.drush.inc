<?php
/**
 * @file
 * Drush commands for taxonomy_server_client module.
 */

/**
 * Implements hook_drush_command().
 */
function taxonomy_server_client_drush_command() {
  $items = array();
  $items['taxonomy-server-client'] = array(
    'description' => 'Get the latest data from taxonomy server.',
    'aliases' => array('taxcli'),
    'options' => array(
      'server' => 'The vocabulary machine name id of the vocabulary that we want to consume from the server.',
      'servervid' => 'The vocabulary vid that we want to consume from the server.',
    ),
    'examples' => array(
      'drush taxcli --server=server_vocab_machine_name --servervid=server_vocabulary_id',
    ),
  );
  return $items;
}

/**
 * Callback function for the drush command to import a vocabulary.
 */
function drush_taxonomy_server_client() {
  drush_log(dt('Taxonomy import starting...'), 'ok');

  // Validate all of the drush options.
  $server_vocabulary_machine_name = drush_get_option('server');
  if (!is_string($server_vocabulary_machine_name)) {
    drush_log(dt('server (server vocabulary machine name) is required.'), 'error');

    return FALSE;
  }

  $server_vocabulary_id = drush_get_option('servervid');
  if (!is_string($server_vocabulary_id)) {
    drush_log(dt('server (server vocabulary vid) is required.'), 'error');

    return FALSE;
  }

  // Use the mapping table to make sure we are updating the correct vocabulary.
  $client_vocabulary_machine_name = taxonomy_server_client_get_mapping($server_vocabulary_machine_name);
  if (!$client_vocabulary_machine_name) {
    drush_log(dt('server (server vocabulary machine name) was not found in taxonomy_server_client_mapping in settings.php.'), 'error');

    return FALSE;
  }

  $format = drush_get_option('format');
  if (!$format) {
    $format = 'json';
  }

  $client_vocabulary_id = _taxonomy_server_client_get_vocabulary_id_by_machine_name($client_vocabulary_machine_name);

  // @TODO: Update server to use machine name instead of vid so we can get the
  // vocabulary from the server.
  $result = taxonomy_server_client_get_vocabulary_by_id($server_vocabulary_id, $format);
  if ($result) {
    $load = taxonomy_server_client_load_vocabulary($client_vocabulary_id, $result, $format);
    if ($load != 'OK') {
      drush_log(dt('Something failed while loading the vocabulary! Message: ' . $load), 'error');
    }
    else {
      drush_log(dt('Taxonomy import complete!'), 'ok');
    }
  }
  else {
    drush_log(dt('Taxonomy import failed to run.'), 'error');
  }
}
