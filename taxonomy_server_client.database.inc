<?php
/**
 * @file
 * All things DB queries.
 */

/**
 * Select tid from uuid_term_data table by uuid.
 *
 * @param $uuid String
 *  The UUID.
 *
 * @return Integer
 *  The TID or FALSE.
 **/
function _taxonomy_server_client_get_tid_by_uuid($uuid) {
  $result = db_select('taxonomy_term_data', 'td')
    ->fields('td', array('tid'))
    ->condition('uuid', $uuid)
    ->execute();

  if (!$result) {
    $var = array(
      '@msg' => 'Function failed: ' . __FUNCTION__,
      '@uuid' => $uuid,
    );
    $message = '@msg @uuid';
    taxonomy_server_client_log($message, 'error', $var);
    return FALSE;
  }

  $record = $result->fetchObject();
  if ($record->tid) {
    return $record->tid;
  }
  else {
    return FALSE;
  }
}

/**
 * Select uuid from uuid_term_data table by tid.
 *
 * @param $tid Integer
 *  The TID.
 *
 * @return Integer
 *  The UUID or FALSE.
 **/
function _taxonomy_server_client_get_uuid_by_tid($tid) {
  $result = db_select('taxonomy_term_data', 'td')
    ->fields('td', array('uuid'))
    ->condition('td.tid', $tid)
    ->execute();

  if (!$result) {
    $var = array(
      '@msg' => 'Function failed: ' . __FUNCTION__,
      '@tid' => $tid,
    );
    $message = '@msg @tid';
    taxonomy_server_client_log($message, 'error', $var);
    return FALSE;
  }

  $record = $result->fetchObject();
  if ($record->uuid) {
    return $record->uuid;
  }
  else {
    return FALSE;
  }
}

/**
 * Delete all entries from taxonomy_server_client_queue with the provided
 *  vocabulary id.
 *
 * @param $vocabulary_id Integer
 *  The vocabulary id.
 *
 * @return
 **/
function _taxonomy_server_client_clear_queue($vocabulary_id) {
  $num_deleted = db_delete('taxonomy_server_client_queue')
    ->condition('vid', $vocabulary_id)
    ->execute();

  // @FIXME: Do we want to log anything?
  // if (!$num_deleted) {
  //   $var = array(
  //     '@msg' => 'Function failed: ' . __FUNCTION__,
  //     '@vid' => $vid,
  //   );
  //   $message = '@msg @vid';
  //   taxonomy_server_client_log($message, 'error', $var);
  // }
}

/**
 * Delete entry from taxonomy_server_client_queue by column provided.
 *
 * @param $value String
 *  The uuid.
 *
 * @return
 **/
function _taxonomy_server_client_delete_from_queue($uuid) {
  $num_deleted = db_delete('taxonomy_server_client_queue')
    ->condition('uuid', $uuid)
    ->execute();

  // @FIXME: Do we want to log anything?
  // if (!$result) {
  //   $var = array(
  //     '@msg' => 'Function failed: ' . __FUNCTION__,
  //     '@column' => $column,
  //     '@value' => $value,
  //   );
  //   $message = '@msg @column @value';
  //   taxonomy_server_client_log($message, 'error', $var);
  // }
}

/**
 * Insert term into taxonomy_server_client_queue.
 *
 * @param $uuid String
 *  The UUID.
 *
 * @param $vocabulary_id Integer
 *  The vocabulary id.
 *
 * @param $tid Integer
 *  The TID.
 *
 * @return
 **/
function _taxonomy_server_client_insert_into_queue($uuid, $vocabulary_id, $tid) {
  $result = db_insert('taxonomy_server_client_queue')
    ->fields(array(
      'uuid' => $uuid,
      'vid' => $vocabulary_id,
      'tid' => $tid,
    ))
    ->execute();

  // @FIXME: Figure out how to catch an error to log.
  // if (!$result) {
  //   $var = array(
  //     '@msg' => 'Function failed: ' . __FUNCTION__,
  //     '@uuid' => $uuid,
  //     '@vocabulary_id' => $vocabulary_id,
  //     '@tid' => $tid,
  //   );
  //   $message = '@msg @uuid @vocabulary_id @tid';
  //   taxonomy_server_client_log($message, 'error', $var);
  // }
}

/**
 * Update taxonomy_server_client_queue TID based on provided UUID.
 *
 * @param $uuid String
 *  The UUID.
 *
 * @param $tid Integer
 *  The TID.
 *
 * @return
 **/
function _taxonomy_server_client_update_queue_tid($uuid, $tid) {
  $num_updated = db_update('taxonomy_server_client_queue') // Table name no longer needs {}
    ->fields(array(
      'tid' => $tid,
    ))
    ->condition('uuid', $uuid)
    ->execute();

  // @FIXME: Do we want to log anything?
  // if (!$num_updated) {
    // $var = array(
    //   '@msg' => 'Function failed: ' . __FUNCTION__,
    //   '@uuid' => $uuid,
    //   '@tid' => $tid,
    // );
    // $message = '@msg @uuid @tid';
    // taxonomy_server_client_log($message, 'error', $var);
  // }
}

/**
 * Update taxonomy_term_data UUID for the provided TID.
 *
 * @param $uuid String
 *  The UUID.
 *
 * @param $tid Integer
 *  The TID.
 *
 * @return
 **/
function _taxonomy_server_client_update_term_data_uuid($uuid, $tid) {
  $num_updated = db_update('taxonomy_term_data') // Table name no longer needs {}
    ->fields(array(
      'uuid' => $uuid,
    ))
    ->condition('tid', $tid)
    ->execute();
  // @FIXME: Do we want to log anything?
  // if (!$num_updated) {
    // $var = array(
    //   '@msg' => 'Function failed: ' . __FUNCTION__,
    //   '@uuid' => $uuid,
    //   '@tid' => $tid,
    // );
    // $message = '@msg @uuid @tid';
    // taxonomy_server_client_log($message, 'error', $var);
  // }
}

/**
 * Returns all queue terms to be deleted.
 *
 * @param $vocabulary_id Integer
 *  The vocabulary id.
 *
 * @return Boolean
 *  The results of the query.
 **/
function _taxonomy_server_client_get_terms_to_delete_from_queue($vocabulary_id) {
  return;
  // @FIXME: This does not seem to work.
  // @FIXME: Rewrite for D7.
  $result = db_query(
    'SELECT td.tid, utd.uuid ' .
    'FROM {uuid_term_data} utd ' .
    'INNER JOIN {term_data} td ON td.tid = utd.tid ' .
    'LEFT JOIN {taxonomy_server_client_queue} tcq ON tcq.uuid = utd.uuid AND tcq.vid = td.vid AND tcq.tid = td.tid ' .
    'WHERE td.vid = %d AND tcq.uuid IS NULL',
    $vocabulary_id
  );

  if (!$result) {
    $var = array(
      '@msg' => 'Function failed: ' . __FUNCTION__,
      '@vocabulary_id' => $vocabulary_id,
    );
    $message = '@msg @vocabulary_id';
    taxonomy_server_client_log($message, 'error', $var);
  }
  $return = array();
  while ($row = db_fetch_object($result)) {
    $return[] = $row;
  }
  return $return;
}

/**
 * Select vid from vocabulary table by machine name (module column).
 *
 * @param $machine_name String
 *  The vocabulary's machine name.  REQUIRED.
 *
 * @return Boolean
 *  The VID or FALSE.
 **/
function _taxonomy_server_client_get_vid_by_machine_name($machine_name) {
  if (!$machine_name) {
    return FALSE;
  }

  $result = db_select('taxonomy_vocabulary', 'v')
    ->fields('v', array('vid'))
    ->condition('v.machine_name', $machine_name)
    ->execute();


  if (!$result) {
    $var = array(
      '@msg' => 'Function failed: ' . __FUNCTION__,
      '@machine_name' => $machine_name,
    );
    $message = '@msg @machine_name';
    taxonomy_server_client_log($message, 'error', $var);
    return FALSE;
  }

  $record = $result->fetchObject();

  if ($record->vid) {
    return $record->vid;
  }
  else {
    return FALSE;
  }
}

/**
 * Select machine name from vocabulary table by vid.
 *
 * @param $vid String
 *  The vocabulary's vid.  REQUIRED.
 *
 * @return Boolean
 *  The machine name or FALSE.
 **/
function _taxonomy_server_client_get_machine_name_by_vid($vid) {
  if (!$vid) {
    return FALSE;
  }

  $result = db_select('taxonomy_vocabulary', 'v')
    ->fields('v', array('machine_name'))
    ->condition('v.vid', $vid)
    ->execute();


  if (!$result) {
    $var = array(
      '@msg' => 'Function failed: ' . __FUNCTION__,
      '@vid' => $vid,
    );
    $message = '@msg @vid';
    taxonomy_server_client_log($message, 'error', $var);
    return FALSE;
  }

  $record = $result->fetchObject();

  if ($record->machine_name) {
    return $record->machine_name;
  }
  else {
    return FALSE;
  }
}
