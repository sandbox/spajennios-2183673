## Taxonomy Server Client README
### Table of Contents
* [Configuring the client]

### Configuring the client

In settings.php, set the following variable to the taxonomy server's endpoint.

```
$conf['taxonomy_server_client_server_uri'] = 'http://yourdomain.com'
$conf['taxonomy_server_client_server_vocabulary_id_endpoint'] = '/taxonomy_server_sample_service/taxonomy_server_vocabulary/';

# @TODO: Configure this once the endpoint is available in the taxonomy_server module.
$conf['taxonomy_server_client_server_vocabulary_machine_name_endpoint'] = '';

# @TODO: Configure this once the endpoint is available in the taxonomy_server module.
$conf['taxonomy_server_client_server_term_endpoint'] = '';

# Boolean value to enable term deletions on the client.
$conf['taxonomy_server_client_delete'] = FALSE;

# Boolean value to enable term synonyms on the client.
$conf['taxonomy_server_client_synonyms'] = TRUE;

# Boolean value to enable term relations on the client.
$conf['taxonomy_server_client_relations'] = FALSE;

# The key represents the vocabulary machine name on the server.
# The value is the vocabulary machine name on the client.
$conf['taxonomy_server_client_mapping'] = array('taxonomy_server_sample_vocabulary' => 'taxonomy_server_sample_vocabulary');
```

You can view these settings here:

```
/admin/settings/taxonomy_server_client
```

When running the client via command line, you MUST get the vid and machine name
of the vocabulary you wish to comsume.

First, view the list of available vocabularies:
```
http://yourdomain.com/taxonomy_server_sample_service/taxonomy_server_vocabulary
```

Second, build your drush command as follows:
```
 drush taxcli --server={vocabulary_machine_name on the server} --servervid={vocabulary_id on the server}
```
This command will find a vocabulary on your client with a matching
vocabulary_machine_name or create a new one with that machine name.
